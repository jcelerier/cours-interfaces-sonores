layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **interfaces sonores**
### module 3: Formats de son, montage, mixage, traitement

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Objet du cours

Apprendre à manipuler les sons au niveau de l'échantillon.

* Rappel: quels sont les caractéristiques d'un fichier son ?
* Rappel: qu'appelle-t-on format PCM ?

---
class: nord-light

# Format mémoire

Dans les langages usuels (C, C++, etc.), les fichiers sons
sont représenté en mémoire sous forme de tableaux.

# Entrelacement (interleaving)

La plupart des formats multicanaux sont entrelacés.
C'est-à-dire que les échantillons alternent entre le canal droite et gauche.

Cela demande un décodage.

Question: pourquoi ?

---
class: nord-light

## Exercice 3.1: générer une forme d'onde sinusoidale

- Télécharger et compiler (avec CMake) la bibliothèque PortAudio:
  https://github.com/PortAudio/PortAudio

- Utiliser la bibliothèque PortAudio pour générer un son, via la méthode des callbacks.

On pourra s'inspirer de https://github.com/PortAudio/portaudio/blob/master/examples/paex_sine.c


---
class: nord-light

## Synthèse avancée

![](assets/ondes.png)

---
class: nord-light

## Exercice 3.2: synthèse avancée

* Proposer des méthodes pour génerer des formes d'ondes:
  - Triangulaires
  - En dent de scie
  - Carrées

* En implémenter une au choix.
* Que se passe-t-il pour une fréquence supérieure à 22khz ? Pourquoi ?

---
class: nord-light

## Lire un fichier son

Deux étapes:
- Le décodage.
- La lecture.

Décodage: passer du format du fichier sur le disque (entrelacé, souvent compressé) à un format que peut comprendre la carte son (non-compressé, non-entrelacé, parfois à virgule flottante).

Les fichiers .wav ont un format très simple (RIFF).

Voir:
- http://www-mmsp.ece.mcgill.ca/Documents/AudioFormats/WAVE/WAVE.html
- https://gist.github.com/Jon-Schneider/8b7c53d27a7a13346a643dac9c19d34f

---
class: nord-light

## Exercice 3.3: décoder un fichier wav (1/2)

- Écrire une structure en C représentant le format RIFF.

On pourra utiliser la fonctionnalité du C des membres "flexibles" de structure:
https://en.wikipedia.org/wiki/Flexible_array_member

- Utiliser `mmap` pour charger le fichier dans la structure.

- Lire le fichier sur la carte son en copiant les échantillons au fur et à mesure des callbacks.

---
class: nord-light

## Exercice 3.4: décoder un fichier wav (2/2)

Cela ne fonctionne pas: pourquoi ?

Réessayer en marquant la définition de la structure comme suit:
```C++
struct __attribute__((packed, aligned(8))) ma_structure {
  ...
}
```

---
class: nord-light

## Exercice 3.5: lire un fichier audio (plus simplement)

On va maintenant réaliser le même exercice avec une bibliothèque plus avancée,
libsndfile.
Cette bibliothèque permet de décoder plusieurs formats: essayer par exemple avec
un format compressé tel que le FLAC.

Attention: mp3 n'est pas disponible, en raison des brevets (bien qu'ils aient expiré l'an dernier).

---
class: nord-light

# Exercice 3.6: montage

Le montage de son peut se résumer à deux opérations:
- Mise en série: un son suivi d'un autre.
- Mise en parallèle: un son joué en même temps qu'un autre.

Jouer deux sons en même temps revient à faire la somme de leurs signaux.
(Cela se prouve via la décomposition de Fourier: on ne fait que rajouter des
sinus !)

1. Lire un fichier son, pendant qu'un son sinusoidal (d'amplitude 1) joue.
   Qu'observe-t-on au niveau du son ?

2. Réaliser une opération de normalisation: on cherche le volume le plus élevé et on divise
   pour qu'il vaille la valeur maximale (1.0 en virugle flottante, 2^15-1 pour du son 16-bits...).


---
class: nord-light

# Exercice 3.7: filtrage

Implémentons maintenant quelques effets simples à appliquer sur nos sons.
- Amplification / gain (vu avec PureData précédemment)
- Distorsion (utiliser tan hyperbolique)
- Filtre passe-bas (x[n] * x[n-1]).
  * Pourquoi x[n] * x[n-1] ?

---
class: nord-light

# Exercice 3.8: filtrage - amélioration du code

On cherche maintenant à appliquer de multiples effets les uns à la suite des autres.

La question de l'architecture se pose: comment peut-on représenter nos effets
de manière à pouvoir les appliquer de manière successive ?

Par exemple, appliquer un gain sur un premier fichier son, et appliquer une distorsion suivie d'un passe-bas sur un second fichier son,
qui jouent en parallèle.

