layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **interfaces sonores**
### module 5: Langages et modèles du temps

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Interactivité

- Du compositeur
- De l'interprète
- Du public

En un mot : `si A alors B`.

Question : combien de temps après ?

On peut rechercher l'**écriture directe** de l'interactivité.

Ou bien concevoir des oeuvres pour lesquelles l'interactivité est **innée**.

---
class: nord-light

# Musique à forme variable

- On abandonne la partition traditionnelle

*Klavierstücke XI*, Karlheinz Stockhausen
![](assets/klavierstucke.jpg)

---
class: nord-light

# Musique à forme variable

- Parfois sujet à interprétation...

![](assets/graphique3.jpg)
*Serenata per un Satellite*, Bruno Maderna

---
class: nord-dark, center

## Quels sont les outils pour permettre aux ordinateurs de comprendre, interpréter, jouer de telles partitions ?

---
class: nord-dark, center

## Quels sont les manières de représenter le temps qui sont assez flexibles pour ce genre de systèmes ?

**Timeline**, **Timeflow**, synchronicité...

---
class: nord-light

# Outils informatique: langages textuels

Très rapidement, les croisements entre informaticiens et musiciens ont
fait se rendre compte à ces derniers que le pouvoir expressif qu'ils désiraient,
était pleinement réalisé par les langages de programmation informatiques.

**Exemples**: ChucK, SuperCollider, Sonic Pi, Ixi, Antescofo...

* https://en.wikipedia.org/wiki/List_of_audio_programming_languages

---
class: nord-light

# Outils informatique: langages visuels

Ces langages, qui demandent une certaine rigueur au niveau de la syntaxe, etc...
sont parfois trop difficiles à approcher pour les musiciens.

Des langages visuels ont été créés.

**Exemples**: ossia score, Max/MSP, PureData, Ascographe ...

**Avantage**: erreurs de syntaxe impossibles:
les éditeurs graphiques ne permettent pas d'arriver dans des cas invalides.


---
class: nord-dark

Inconvénient:

![](assets/puredata.png)

---
class: nord-light

# Architecture pour les langages audio

Bien que différents, ces langages partagent une représentation commune de leur modèle de données:
le graphe à flot de données (*dataflow graph*).

Il existe de nombreuses manières de définir ces graphes : l'espace des possibles est vaste.

* Fonctionnement des ports ?
* Graphe synchrone / asynchrone ?
* Gestion des contrôles ?
* Calcul précis à l'échantillon / au tick ?
* Etc etc...

---
class: nord-light

# Méthode générale

* Créer un modèle de données sous forme de graphe.
* Créer un langage dont les primitives vont créer des nœuds de ce graphe.
* Exécuter le graphe à intervalle régulier
* Important: il faut avoir une notion précise de **transaction**, pour la modification de ce graphe.

**Exemple**: SuperCollider: le langage est désynchronisé du graphe : les messages du langage sont envoyés par réseau au processus qui contient le modèle de données de graphe.

---
class: nord-light

# Types de nœuds courants

- Lecture de fichier son
- Lecture de fichier midi
- Courbe d'automation
- Effet audio (distorsion, reverb, chorus...)
- Synthétiseur

---
class: nord-light

# Exécution

* À chaque tick d'exécution (par exemple à chaque fois que le *callback* de la carte son est appelé), on doit exécuter tous les nœuds *actifs*. Par exemple, les pistes non mutées, etc., dans le bon ordre.

* C'est-à-dire que si deux effets de guitare sont l'un après l'autre (par exemple distortion, puis reverb), l'algorithme d'exécution doit les exécuter dans le bon ordre (sinon le nœud de reverb n'aura pas les bonnes données sur lesquelles appliquer son algorithme).

---
class: nord-light

# Tri topologique

* L'algorithme le plus important est le tri topologique du graphe.
* Les graphes sont dirigés, et généralement acycliques. On veut trouver l'ordre dans lequel chaque nœud du graphe doit s'exécuter.
* Tri topologique: liste des nœuds dans l'ordre dans lequel il doit s'exécuter.

La méthode en une phrase: pile + parcours récursif.

---
class: nord-light

# TP: faisons notre propre graphe ! (base)

* Définir une API pour les nœuds du graphe.
  - Comment fonctionne l'exécution ?
  - Comment représente-t-on le temps ?
  - Comment sait-on ou on en est de l'exécution de chaque nœud ? (fichiers son !)
  - Comment les nœuds échangent-ils des données ?

* Chaque nœud doit lire depuis ses entrées et écrire dans ses sorties.

* Implémenter un nœud de test (mock) pour vérifier le fonctionnement.

---
class: nord-light

# TP: faisons notre propre graphe ! (nœuds)

* Implémenter quelques nœuds
  - Synthèse (on l'a déjà)
  - Lecture de MIDI (on l'a quasiment)
  - Lecture de fichier son (on l'a quasiment)
  - Effet: distorsion (`sqrt` ou `tanh` sur le signal)
  - Comment sait-on ou on en est de l'exécution de chaque nœud ? (fichiers son !)
  - Comment les nœuds échangent-ils des données ?

---
class: nord-light

# TP: faisons notre propre graphe ! (exécution)

* Implémenter le mécanisme d'exécution:
  1. Tri topologique
  2. Exécution de chaque nœud un par un.

* Réflechir à l'allocation des buffers audio.

* Les derniers nœuds de la chaîne doivent écrire dans le buffer de la carte son.

---
class: nord-dark

# 99% des logiciels de musique consistent en des variations sur ce thème + des interfaces graphiques ou textuelles.



