layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **interfaces sonores**
### module 4: représentation symbolique de la musique

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Objet du cours

Apprendre la manière dont les données musicales sont manipulées par les logiciels de création de musique, éditeurs de partitions, jeux vidéos des années 80 et 90...

---
class: nord-light

# Représentation symbolique

La musique est stockée sous forme de symboles plutôt que de formes d'ondes:
une manière pompeuse de dire **format de données**.

![](assets/clairdelune.png)

---
class: nord-light

# Représentation symbolique

Ne contient pas toutes les informations nécessaires à la performance: une partie vient
des connaissances et du feeling du musicien.

Vrai joueur:
- https://www.youtube.com/watch?v=fZrm9h3JRGs

Partition interprétée directement:
- https://www.youtube.com/watch?v=JQkPWmo_V6o

---
class: nord-light

# Représentations symboliques principales 1/2

- Représentations ad-hoc: **Trackers**

Utilisés dans les jeux vidéos (quasiment tous jusqu'à la PlayStation 1).
Certains musiciens s'en servent en raison de la grande accessibilité de ces logiciels.

- **Exemple**: https://www.youtube.com/watch?v=dKvreHmHKds
- **Exemple**: https://toriena321.bandcamp.com/track/reloading

- Chaque tracker utilise son propre format... MOD, XM, IT.
Notion de **canal** (synthèse utilisée), de **note**, d'**effet** et de **mesure** (groupe de notes).
- Un canal («son») ne peut jouer qu'une note à la fois: opère comme
l'objet [osc~] dans PureData par exemple. Vient des limitations matérielles
des premières consoles (ex. GameBoy: 4 canaux).

---
class: nord-light

# Représentations symboliques principales 2/2

- Représentation pro: **MIDI**
- Introduit dans les synthétiseurs en 1983.
- Mis à jour (enfin !) l'an dernier avec MIDI 2.
- 16 canaux

Plusieurs types de messages:
* Note On, Note Off
* Control Change, Pitch Bend, Aftertouch
* Program Change
* System Exclusive (SYSEX)

---
class: nord-light

# Représentations symboliques principales 3/2

Chaque logiciel va certainement disposer de son propre modèle de données interne,
souvent plus précis...

- Exemple: Norme de plug-ins audio Steinberg VST3

https://steinbergmedia.github.io/vst3_doc/vstinterfaces/MidiVst3About.html


---
class: nord-light

# Données MIDI

Chaque message MIDI a un format spécifique et très concis, qui permet à un synthétiseur
de le traiter rapidement (il fallait que cela soit « **temps-réel** » avec des CPU bon marché de 1983 !).

Une note midi: quel **canal** ? quelle **hauteur** ? quelle **nuance** (vélocité) ? Le MIDI est « piano-centric ».
```cpp
                note on
        status  |
             |  |
             v  v
Status byte: 1001CCCC < canal utilisé (4 bits)

          data
             |
             v
Pitch:       0PPPPPPP < pitch utilisé (7 bits)
Velocity:    0VVVVVVV < vélocité utilisée (7 bits)
```



---
class: nord-light

# Données MIDI (suite)

Note On ne contient **pas d'information intrinsèque de durée**.
La note correspond uniquement à l'information « la touche de clavier est pressée ».

Pour que la note se termine, il faut envoyer un message **Note Off**:
```cpp
                note off
        status  |
             |  |
             v  v
Status byte: 1000CCCC < canal utilisé (4 bits)

          data
             |
             v
Pitch:       0PPPPPPP < pitch utilisé (7 bits)
Velocity:    0VVVVVVV < vélocité utilisée (7 bits, «release velocity», très souvent 0)
```


---
class: nord-light

# Données MIDI (suite)

Que se passe-t-il si un clavier est débranché après qu'un Note On, et avant qu'un Note Off soit envoyé ?

---
class: nord-light

# Données MIDI (suite)

![](assets/panic1.png)
---
class: nord-light

# Données MIDI (suite)

![](assets/panic2.jpg)

---
class: nord-light

# MIDI avancé

- **Running status**: permet d'économiser un octet par message si on envoie une succession de messages du même type.
Par exemple, trois messages « Note On » sur le canal 2: seul le premier contient le « status byte ».
L'octet de status est identifié par le MSB (most significant bit) qui est mis à 1.
Les autres octets, dits "data bytes" sont identifiés par le MSB qui est mis à 0.

- **Control change**: permet de contrôler des paramètres d'un synthétiseur, d'un effet... 127 disponibles.
- **Program change**: permet de changer le son de l'instrument en cours de route: passer d'une guitare à un saxophone par exemple. 127 disponibles.
- **Aftertouch / channel pressure**: permet de moduler l'intensité d'une note en appuyant sur le clavier plus ou moins fort après avoir envoyé un Note On.


---
class: nord-light

# MIDI files

- MIDI à la base prévu pour du temps-réel: on branche un clavier à un synthétiseur qui génère du son au fur et à mesure que les notes sont pressées.
- Facilement extensible à du stockage sous forme de fichier, avec la notion de timestamp (estampille).
- Norme SMF (Standard MIDI File).

https://www.midi.org/specifications-old/item/standard-midi-files-smf

---
class: nord-light

# TP MIDI

* Objectif: arriver à lire un fichier MIDI dans notre synthétiseur de la dernière fois.

Étapes:
- Charger un fichier MIDI
- Définir un modèle de données
- Connecter au synthétiseur du TP précédent:

https://gitlab.com/jcelerier/cours-interfaces-sonores/-/raw/master/public/corrections/ex3.2.cpp

- Lire les notes une par une de manière monophonique
- Lire plusieurs notes en parallèle (polyphonie)
- Respecter les indications de temps

---
class: nord-light

# TP MIDI: lire un fichier MIDI

On va utiliser la bibliothèque rtmidi17.

https://github.com/jcelerier/RtMidi17

Includes:
```cpp
#define RTMIDI17_HEADER_ONLY 1
#include <rtmidi17/reader.hpp>
#include <iostream>
#include <fstream>
```

Compilation:
```bash
g++ -Wno-multichar -I /chemin/vers/rtmidi17/include -std=c++17 exo.cpp
```

---
class: nord-light

# TP MIDI: lire un fichier MIDI

Exemple: http://www.piano-midi.de/midis/debussy/deb_clai_format0.mid

```cpp
std::vector<uint8_t> bytes;
std::ifstream file{"/chemin/vers/fichier.mid", std::ios::binary};
bytes.assign(std::istreambuf_iterator<char>(file), std::istreambuf_iterator<char>());

rtmidi::reader reader;
reader.parse(bytes);
// reader contient maintenant les informations contenues dans le fichier midi.
```


---
class: nord-light

# TP MIDI: synthèse monophonique (comme les trackers)

On va commencer par lire uniquement les Note On, et changer la hauteur et le volume du synthétiseur en conséquence.

- Mettre le code qui charge un fichier MIDI en tant que fonction.
  Pour cet example simple, on pourra accepter de mettre la structure de données en variable globale.
- Pour chaque évènement MIDI Note On, lire la note correspondante pendant 500 millisecondes.
  (à calculer avec fréquence d'échantillonnage et taille de buffer).
- Calculer la fréquence à jouer, à partir de la note MIDI (qui va de 0 (Do -1) à 127 (Sol 9): `440 * 2^((x-69) / 12)`)
- Calculer le volume et multiplier la sortie de l'algorithme de synthèse par cette valeur.

---
class: nord-light

# TP MIDI: synthèse polyphonique

On veut maintenant implémenter un mécanisme de « voix » : plusieurs notes différentes peuvent jouer en même temps.
On laisse les « note on » tourner jusqu'à ce qu'un « note off » correspondant soit atteint.

---
class: nord-light

# TP MIDI: synthèse avec gestion du temps

- On veut maintenant prendre en compte les durées correctes des notes.
- Les fichiers MIDI stockent cette information de manière relative (durée depuis la note précédente en ticks).
