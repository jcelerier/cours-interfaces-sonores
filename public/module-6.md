layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **interfaces sonores**
### module 6: TP: synthétiseur polyphonique

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Déroulement

Le TP fera partie du contrôle continu. Le cadre est celui de la **synthèse soustractive** (pas additive, ni FM).

1. Qu'est-ce qu'un **synthétiseur polyphonique** ? À quoi ressemblerait un synthétiseur « moyen » du commerce, quelles seraient ses caractéristiques ?

2. Une fois que les caractéristiques que l'on désire sont établies, les figer sous forme de **diagramme**.

3. À partir du code réalisé sur les dernières séances, et des ressources données en page d'après, l'**implémenter**.

---
class: nord-light

# Conseils

0. On pourra évidemment réutiliser les cours corrections des fois précédentes: ce TP sert à synthétiser ces connaissances.

1. Fixer l'**architecture** avant tout, et faire des nœuds simples pour tester.

2. S'assurer de **toujours** avoir du son qui sort quand on lance le programme: plus on passe de temps sans avoir de son, plus ce sera complexe à débuguer.

3. **BAISSER LE VOLUME SI VOUS UTILISEZ DES ÉCOUTEURS**. Pour ne pas perdre d'audition prématurément, on pourra mettre en fin de chaine un nœud qui multiplie la sortie par 0.8 et *limite* le signal à ces valeurs (c'est-à-dire que tous les échantillons seront compris dans l'interval d'amplitude [-0.8; 0.8]).

---
class: nord-light

# Ressources: filtres

- De nombreux effets nécessitent des **filtres**: passe-haut, passe-bas, low-pass, all-pass. On pourra utiliser cette bibliothèque pour les implémenter très simplement: https://github.com/olilarkin/DSPFilters

```cmake
dspfilters:
    g++ -fPIC -O3 -march=native DSPFilters/source/* -IDSPFilters/include -shared -o libdspfilters.so
```

---
class: nord-light

# Ressources: description d'effets en MATLAB

- http://www.cs.cf.ac.uk/Dave/CM0268/PDF/10_CM0268_Audio_FX.pdf
- https://ccrma.stanford.edu/~orchi/Documents/DAFx.pdf
- https://github.com/juandagilc/Audio-Effects
