layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **interfaces sonores**
### module 10: greffons logiciels


Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light

# Qu'est-ce qu'un greffon

En anglais: *plug-in*.

* Un bout de code  que l'on introduit dans un autre logiciel.

---
class: nord-light

# APIs, ABIs

* Qu'est-ce qu'une API ?
* Qu'est-ce qu'une ABI ?
* Quelle est la différence ?

---
class: nord-light

# ABI "C"

* Raccourci pour dire "ABI du système d'exploitation".
* Ne supporte que les fonctionnalités du langage C: pointeurs, pointeurs de fonction, types simples (int, char, float...).

---
class: nord-light

# ABI "COM"

* Introduit par Microsoft dans les années fin-80.
* Permet d'utiliser quelques fonctionnalités du C++ : héritage (non multiple !), méthodes virtuelles. Pas de support de la surchage de fonctions.

---
class: nord-light

# Example 1 : plug-ins VST

Norme VST: développée par Steinberg dans les années 90.

* VST 2: ABI C.
  * Example: https://gitlab.com/jcelerier/audio-plugin-example
* VST 3: ABI COM. Example:
  * Example: https://github.com/steinbergmedia/vst3_public_sdk/blob/c3948deb407bdbff89de8fb6ab8500ea4df9d6d9/samples/vst/again/source/againsimple.cpp

* Quels sont les types supportés ?

---
class: nord-light

# Exercice

À partir du code https://gitlab.com/jcelerier/audio-plugin-example
* Le modifier pour en faire un filtre passe-bas.

* Example de filtre passe-bas: `x`: entrée, `y`: sortie. `a` est un paramètre.

```c++
y[n] = (1 − a) * y[n−1] + a * (x[n] + x[n−1]) / 2
```

---
class: nord-light

# Example 2 : externals PureData

* Example:

- https://github.com/pure-data/externals-howto

* Quels sont les types supportés ?
* Sûreté de typage ? Comment sait-on que l'on envoie le bon type au bon inlet / outlet ?

---
class: nord-light

# Un point sur Faust

Faust est une solution possible à ce problème:

Un programme Faust peut être traduit automatiquement en code VST, PureData, etc... via des fichiers d'architecture.

Le code Faust étant lui-même fortement typé, il y a beaucoup moins de chance d'avoir des erreurs de type à l'exécution.

---
class: nord-light

# Example 3 : nœuds ossia score simples

* Ouvrir score (sous Mac ou Linux)
* Ajouter le C++ JIT process
* Analyser le code.
* Quels sont les types supportés ?

---
class: nord-light

# Example 4 : nœuds ossia score avancés

* Regarder le code https://github.com/ossia/score/blob/master/src/plugins/score-plugin-fx/Fx/Gain.hpp
* Quels sont les types supportés ?
* Quelle est la différence avec le code précédent ?


---
class: nord-light

# Example 5 : le futur ?

* Difficulté principale: on possède des données au moment de la compilation.
* On veut les garder à l'exécution (nom des ports, etc.) sans dupliquer de code.
* Quelle technique pourrait-on utiliser, par exemple dans les langages Java, Javascript ou C# ?

---
class: nord-light

# Example 5 : le futur ?

https://cppx.godbolt.org/

---
class: nord-light

# Exercice

* Porter le code de filtre de tantôt dans l'API avancée de score.
* Proposer la modification sur Github, sur le dépôt de score, en rajoutant le fichier ici, via l'interface de Pull Request:
   https://github.com/ossia/score/tree/master/src/plugins/score-plugin-fx/Fx
* Le code le plus propre sera `mergé` ;-)

