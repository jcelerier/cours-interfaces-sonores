layout: true
class: typo, typo-selection

---

count: false
class: left, middle, nord-light

# **interfaces sonores**

Jean-Michaël Celerier

mail: <jeanmichael.celerier@gmail.com>

site: <https://jcelerier.name>

---
class: nord-light
<br/><br/>

------

Module 3: [Formats de son, montage, mixage, traitement](module-3.html)

------

Module 4: [MIDI en C et traitement du signal](module-4.html)

------

Module 5: [Langages et modèles du temps](module-5.html)

------

Module 6: [TP: Synthétiseur polyphonique](module-6.html)

------

Module 10: [Greffons logiciels](module-7.html)
