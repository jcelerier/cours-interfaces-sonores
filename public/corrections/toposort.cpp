#include <iostream>
#include <vector>

using vertex_t = int;
using graph_t = std::vector<std::vector<vertex_t>>;

void add_edge(graph_t& graph, vertex_t from, vertex_t to)
{
  graph[from].push_back(to);
}

struct topological_sort_impl {
  const graph_t &graph;

  struct toposort_state {
    explicit toposort_state(vertex_t V) noexcept : stack{}, visited(V, false) {
      stack.reserve(V);
    }

    std::vector<vertex_t> stack;
    std::vector<bool> visited;
  };

  void rec(vertex_t vtx, toposort_state &st) const noexcept {
    st.visited[vtx] = true;

    for (auto &e : graph[vtx])
      if (!st.visited[e])
        rec(e, st);

    st.stack.push_back(vtx);
  }

  std::vector<vertex_t> operator()() const noexcept {
    const vertex_t V = graph.size();
    toposort_state st{V};

    for (vertex_t i = 0; i < V; i++)
      if (st.visited[i] == false)
        rec(i, st);

    return st.stack;
  }
};

std::vector<vertex_t> topological_sort(const graph_t& graph)
{
  return topological_sort_impl{graph}();
}

int main() {
  graph_t graph(6);

  add_edge(graph, 5, 2);
  add_edge(graph, 5, 0);
  add_edge(graph, 4, 0);
  add_edge(graph, 4, 1);
  add_edge(graph, 2, 3);
  add_edge(graph, 3, 1);

  const auto sorted = topological_sort(graph);

  for (auto it = sorted.rbegin(); it < sorted.rend(); ++it)
    std::cout << *it << " ";

  return 0;
}