
#include <cstdio>
#include <cmath>
#include <iostream>
#include <string_view>

#include <portaudio.h>

static const constexpr double fs = 44100.;

struct audio_state {
   int phase_l{};
   int phase_r{};
};
static int audio_callback( const void *inputBuffer, void *outputBuffer,
                            unsigned long framesPerBuffer,
                            const PaStreamCallbackTimeInfo* timeInfo,
                            PaStreamCallbackFlags statusFlags,
                            void *userData )
{
  using namespace std;
  float** out = (float**)outputBuffer;
  float* out_l = out[0];
  float* out_r = out[1];

  audio_state& state = *reinterpret_cast<audio_state*>(userData);

  // Rappel: sin(2 * pi * f * i / fs);
  for(unsigned long i = 0; i < framesPerBuffer; i++)
  {
      out_l[i] = 0.5 * sin(2 * M_PI * 300.0 * state.phase_l++ / fs);
      out_r[i] = 0.5 * sin(2 * M_PI * 400.0 * state.phase_r++ / fs);
  }

  return paContinue;
}

int get_default_device()
{
  using namespace std::literals;
  for (int i = 0; i < Pa_GetDeviceCount(); i++)
  {
    auto info = Pa_GetDeviceInfo(i);
    std::cerr << info->name << std::endl;
    if(info->name == "pulse"sv)
      return i;
  }
  return Pa_GetDefaultOutputDevice();
}

int main(void)
{
  audio_state state;

  Pa_Initialize();

  PaStreamParameters outputParameters{};
  outputParameters.device = get_default_device();
  outputParameters.channelCount = 2;
  outputParameters.sampleFormat = paFloat32 | paNonInterleaved;
  outputParameters.hostApiSpecificStreamInfo = nullptr;

  PaStream *stream{};
  Pa_OpenStream(&stream, nullptr,  &outputParameters,
                 48000, 128, paClipOff, audio_callback, &state);
  Pa_StartStream(stream);

  Pa_Sleep(15000);

  Pa_StopStream(stream);
  Pa_CloseStream(stream);
  Pa_Terminate();

  return 0;
}