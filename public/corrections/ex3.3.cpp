#include <cstdint>
struct wav_header {
  static constexpr int nc = 1;
  static constexpr int F = 44100;
  static constexpr int ns = 1;
  static constexpr int M = 4;

  // RIFF Header
  char ckID[4] = {'R', 'I', 'F', 'F'};
  int cksize =  4 + 48 + 12 + (8 + M*nc*ns + 0);
  char WAVEID[4] = {'W', 'A', 'V', 'E'};

  // Format Header
  char fmt[4] = { 'f', 'm', 't', ' ' };
  int fmt_cksize = 18;
  short wFormatTag = 3;
  short nChannels = nc;
  int nSamplesPerSec = F;
  int nAvgBytesPerSec = F * M * nc;
  short nBlockAlign = M * nc;
  short wBitsPerSample = 8 * M;
  short cbSize = 0;

  // Facts
  char factID[4] = {'f', 'a', 'c', 't'};
  int factSize = 4;
  int dwSampleLength = nc * ns;

  // Data
  char data_header[4] = { 'd', 'a', 't', 'a' };
  int data_bytes = 4 * 1 * 32; // change that

  float v[M*nc*ns] = { 0.1, 0.2, 0.3, 0.4 };
};
